@extends('layout.master')

@section('Judul')
CAST FILM
@endsection

@section('content')

<a href="/cast/create" class="btn btn-primary mb-3">Tambah Data</a>

<table class="table table-dark">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Bio</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key => $item)
            <tr>
               <td>{{$key + 1}}</td> 
               <td>{{$item->nama}}</td>
               <td>{{$item->umur}}</td>
               <td>{{$item->bio}}</td>
               <td>
                    <form action="/cast/{{$item->id}}" method="POST">
                    <a href="/cast/{{$item->id}}" class="btn btn-success btn-sm">Detail</a>
                    <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                        @method('Delete')
                        @csrf

                        <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                    </form>
               </td>
            </tr>
        @empty
        <tr>
            <td>data belum tersedia</td>
        </tr> 
        @endforelse
    </tbody>
</table>

@endsection