@extends('layout.master')

@section('Judul')
Halaman Form
@endsection

@section('content')
    <h1>Media Online</h1>
    <h1>Buat Account Baru!</h1>
    <h1>Sign up form</h1>

    <form action="/welcome"method="post">
        @csrf
        <p>First Name:</p>
        <input type="text" name="nama"><br><br>
        <p>Last Name:</p>
        <input type="text" name="nama"><br><br>
        <Label>Alamat</label><br>
        <textarea name="address" cols="30" rows="10"></textarea><br><br>
        <p>Gender:</p>
        <input type="radio" name="gender">Male<br>
        <input type="radio" name="gender">Female<br>
        <input type="radio" name="gender">Other<br>
        <p>Nationallity:</p>
        <select>
            <option>Indonesia</option>
            <option>Malaysia</option>
            <option>Brunei</option>
        </select>
        <p>Language Spoken:</p>
        <input type="checkbox">Bahasa Indonesia<br>
        <input type="checkbox">English<br>
        <input type="checkbox">Other<br><br>
        </select>
        <Label for="">Bio</label><br><br>
        <textarea name="bio" cols="30" rows="10"></textarea><br><br>
        <input type="submit" value="Kirim">
    </form>
@endsection
